<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Producto extends Model
{
    use HasFactory;

    //Relacion con los detalles de la Compra
    public function CompraDetalles():HasMany{
        return $this->hasMany(ComprasDetalles::class);
    }
}
