<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Compra extends Model
{
    use HasFactory;


    //Relacion pertenece a user BelongsTo
    public function user():BelongsTo{
        return $this->belongsTo(User::class);
    }

    //Relacion con los Detalles de la Compra
    public function detalles():HasMany{
        return $this->hasMany(ComprasDetalles::class);
    }
}
