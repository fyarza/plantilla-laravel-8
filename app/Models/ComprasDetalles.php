<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ComprasDetalles extends Model
{
    use HasFactory;

    //Relacion con la Cabecera de la Compras
    public function compra():BelongsTo{
        return $this->belongsTo(Compra::class);
    }

    //Relacion con el producto 
    public function producto():BelongsTo{
        return $this->belongsTo(Producto::class);
    }
}
