<?php

namespace App\Http\Controllers\Api;



class Controller
{
   /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="OpenApi Documentacion",
     *      description="Documentación Api de recursos para la integración de nuevos sistemas",
     *      @OA\Contact(
     *          email="federicoyarza295@gmail.com"
     *      ),
     *      @OA\License(
     *          name="Apache 2.0",
     *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *      )
     * )
     *
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="API Server"
     * )
     *
     */
}
