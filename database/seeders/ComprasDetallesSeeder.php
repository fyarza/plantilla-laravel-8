<?php

namespace Database\Seeders;

use App\Models\ComprasDetalles;
use Illuminate\Database\Seeder;

class ComprasDetallesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ComprasDetalles::factory(100)->create();
    }
}
