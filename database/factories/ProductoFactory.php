<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->name(),
            'impuesto' => $this->faker->numberBetween(1,15),
            'precio' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL)
        ];
    }
}
