<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ComprasDetallesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'compra_id'=> $this->faker->numberBetween(1,100),
            'producto_id' => $this->faker->numberBetween(1,50),
            'cantidad'=> $this->faker->numberBetween(1,100),
            'precio' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL)
        ];
    }
}
