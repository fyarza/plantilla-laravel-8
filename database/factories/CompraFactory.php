<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CompraFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->numberBetween(1,100),
            'subtotal' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            'total' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            'total_impuesto' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
        ];
    }
}
